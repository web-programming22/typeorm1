import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const productRepostitory = AppDataSource.getRepository(Product)

    const products = await productRepostitory.find()
    console.log("Loaded products: ", products)

    const updatedProduct = await productRepostitory.findOneBy({id:1})
    console.log(updatedProduct)
    updatedProduct.price=100
    await productRepostitory.save(updatedProduct)
    



}).catch(error => console.log(error))
